/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseojardininfantil.controlador;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import paseojardininfantil.controlador.utilidades.JsfUtil;

/**
 *
 * @author carloaiza
 */
@Named(value = "beanLogin")
@SessionScoped
public class BeanLogin implements Serializable {

    private String usuario;

    /**
     * Creates a new instance of BeanLogin
     */
    public BeanLogin() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String ingresarlistaSE() {
        if (usuario.equals("salgado")) {
            return "irListaSE";
        }
        JsfUtil.addErrorMessage("Usted no está autorizado");
        return null;
    }

    public String ingresarlistaDE() {
        if (usuario.compareTo("salgado") == 0) {
            return "irListaDE";
        }
        JsfUtil.addErrorMessage("Usted no está autorizado");
        return null;
    }
    
    public String ingresarlistaCircular() {
        if (usuario.compareTo("salgado") == 0) {
            return "irListaCircular";
        }
        JsfUtil.addErrorMessage("Usted no está autorizado");
        return null;
    }

}
