/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseojardininfantil.controlador;

import jardininfantillistase.controlador.ListaDECircular;
import jardininfantillistase.modelo.Infante;
import jardininfantillistase.modelo.NodoDE;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.chart.PieChartModel;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import paseojardininfantil.controlador.utilidades.JsfUtil;

/**
 *
 * @author SALGADO
 */
@Named(value = "beanListaCircular")
@SessionScoped
public class BeanListaCircular implements Serializable {

    private boolean deshabilitarNuevo = true;

    private ListaDECircular listaCircular = new ListaDECircular();

    private NodoDE infanteMostrar = new NodoDE(new Infante());

    private DefaultDiagramModel model;

    private boolean esCabeza;

    private List<Infante> listaInfantesEliminados;
    
     private PieChartModel pieModel1;

    private int posicionAEliminar = listaCircular.devolverNumeroAlAzar();

    private Infante infanteSeleccionado;

    private boolean deshabilitarJuego = false;

    /**
     * Creates a new instance of BeanListaCircular
     */
    public BeanListaCircular() {
    }

    @PostConstruct
    public void llenarInfante() {
        listaCircular.adicionarNodo(new Infante("Pipe", (byte) 4, 'M'), true);
        listaCircular.adicionarNodo(new Infante("Salome", (byte) 6, 'F'), false);
        listaCircular.adicionarNodo(new Infante("andres", (byte) 5, 'M'), false);
        listaCircular.adicionarNodo(new Infante("Sara", (byte) 4, 'F'), false);
        listaCircular.adicionarNodo(new Infante("Sebas", (byte) 1, 'M'), false);
        listaInfantesEliminados = new ArrayList<>();
        pintarLista();
        createPieModel1();
    }

    public PieChartModel getPieModel1() {
        return pieModel1;
    }

    public void setPieModel1(PieChartModel pieModel1) {
        this.pieModel1 = pieModel1;
    }

    public boolean isDeshabilitarJuego() {
        return deshabilitarJuego;
    }

    public void setDeshabilitarJuego(boolean deshabilitarJuego) {
        this.deshabilitarJuego = deshabilitarJuego;
    }

    public Infante getInfanteSeleccionado() {
        return infanteSeleccionado;
    }

    public void setInfanteSeleccionado(Infante infanteSeleccionado) {
        this.infanteSeleccionado = infanteSeleccionado;
    }

    public int getPosicionAEliminar() {
        return posicionAEliminar;
    }

    public void setPosicionAEliminar(int posicionAEliminar) {
        this.posicionAEliminar = posicionAEliminar;
    }

    public List<Infante> getListaInfantesEliminados() {
        return listaInfantesEliminados;
    }

    public void setListaInfantesEliminados(List<Infante> listaInfantesEliminados) {
        this.listaInfantesEliminados = listaInfantesEliminados;
    }

    public void setListaInfantesEliminados(ArrayList<Infante> listaInfantesEliminados) {
        this.listaInfantesEliminados = listaInfantesEliminados;
    }

    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public ListaDECircular getListaCircular() {
        return listaCircular;
    }

    public void setListaCircular(ListaDECircular listaCircular) {
        this.listaCircular = listaCircular;
    }

    public NodoDE getInfanteMostrar() {
        return infanteMostrar;
    }

    public void setInfanteMostrar(NodoDE infanteMostrar) {
        this.infanteMostrar = infanteMostrar;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public boolean isEsCabeza() {
        return esCabeza;
    }

    public void setEsCabeza(boolean esCabeza) {
        this.esCabeza = esCabeza;
    }

//---------------------
//METODOS
//------------------
    public void habilitarCrearInfante() {
        deshabilitarNuevo = false;
        infanteMostrar = new NodoDE(new Infante());
    }

    public void guardarInfante() {
        listaCircular.adicionarNodo(infanteMostrar.getDato(), esCabeza);
        infanteMostrar = new NodoDE(new Infante());
        deshabilitarNuevo = true;
        pintarLista();
        createPieModel1();
        deshabilitarJuego = false;
        JsfUtil.addSuccessMessage("Infante guardado con exito");
    }

    public void reingresarNiño() {
        listaInfantesEliminados.remove(infanteSeleccionado);
        listaCircular.adicionarNodo(infanteSeleccionado, true);
        infanteSeleccionado = null;
        createPieModel1();
        pintarLista();
    }

    public void cancelarGuardado() {
        deshabilitarNuevo = true;
    }

    public void juagarALaDerecha() {
        infanteSeleccionado = listaCircular.jugarDerecha(posicionAEliminar);
        listaInfantesEliminados.add(infanteSeleccionado);
        if (listaCircular.contarNodoFemenino() == 0) {
            JsfUtil.addSuccessMessage("GANARON LOS NIÑOS");
            deshabilitarJuego = true;
        }
        if (listaCircular.contarNodoMasculino() == 0) {
            JsfUtil.addSuccessMessage("GANARON LAS NIÑAS");
            deshabilitarJuego = true;
        }
        infanteSeleccionado = null;
        createPieModel1();
        pintarLista();
    }

    public void juagarALaIzquierda() {
        infanteSeleccionado = listaCircular.jugarIzquierda(posicionAEliminar);
        listaInfantesEliminados.add(infanteSeleccionado);
        if (listaCircular.contarNodoFemenino() == 0) {
            JsfUtil.addSuccessMessage("GANARON LOS NIÑOS");
            deshabilitarJuego = true;
        }
        if (listaCircular.contarNodoMasculino() == 0) {
            JsfUtil.addSuccessMessage("GANARON LAS NIÑAS");
            deshabilitarJuego = true;
        }
        infanteSeleccionado = null;
        createPieModel1();
        pintarLista();
    }
    
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
         
        pieModel1.set("Niños", listaCircular.contarNodoMasculino());
        pieModel1.set("Niñas", listaCircular.contarNodoFemenino());
        
         
        pieModel1.setTitle("Porcentaje de niños");
        pieModel1.setLegendPosition("w");
    }

    public void pintarLista() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);

        StateMachineConnector connector = new StateMachineConnector();
        connector.setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
        connector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
        model.setDefaultConnector(connector);

        //adicionar los elementos 
        if (listaCircular.getCabeza() != null) {
            NodoDE temp = listaCircular.getCabeza();
            int posX = 2;
            int posY = 2;

            do {
                posX = posX + 7;
                posY = posY + 7;
                Element ele = new Element(temp.getDato().getNombre(), posX + "em", posY + "em");

                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.AUTO_DEFAULT));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.AUTO_DEFAULT));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.AUTO_DEFAULT));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.AUTO_DEFAULT));

                model.addElement(ele);
                temp = temp.getSiguiente();

            } while (temp != listaCircular.getCabeza());

            for (int i = 0; i <= model.getElements().size()-1; i++) {

                if (i == listaCircular.contarNodos()-1) {
                    model.connect(createConnection(model.getElements().get(i).getEndPoints()
                            .get(1), model.getElements().get(0).getEndPoints().get(0), "Ant  Sig"));

                    model.connect(createConnection(model.getElements().get(0).getEndPoints()
                            .get(3), model.getElements().get(i).getEndPoints().get(2), "Ant  Sig"));
                    return;
                }
                model.connect(createConnection(model.getElements().get(i).getEndPoints()
                        .get(1), model.getElements().get(i + 1).getEndPoints().get(0), "Ant  Sig"));

                model.connect(createConnection(model.getElements().get(i + 1).getEndPoints()
                        .get(3), model.getElements().get(i).getEndPoints().get(2), "Ant  Sig"));
            }

        }

    }

    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));
        if (label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }

        return conn;
    }

}
