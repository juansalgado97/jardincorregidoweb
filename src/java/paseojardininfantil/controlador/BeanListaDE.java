/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseojardininfantil.controlador;

import jardininfantillistase.controlador.ListaDE;
import jardininfantillistase.modelo.Infante;
import jardininfantillistase.modelo.NodoDE;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import paseojardininfantil.controlador.utilidades.JsfUtil;

/**
 *
 * @author SALGADO
 */
@Named(value = "beanListaDE")
@SessionScoped
public class BeanListaDE implements Serializable {

    private boolean deshabilitarNuevo = true;

    private ListaDE listaDE = new ListaDE();

    private NodoDE infanteMostrar = new NodoDE(new Infante());

    private DefaultDiagramModel model;

    private int posicion;

    private boolean deshabilitarPrimero = false;

    private boolean deshabilitarSiguiente = false;

    private boolean deshabilitarAnterior = false;

    private boolean deshabilitarUltimo = false;
    
    private boolean deshabilitarEliminar = false;
    
    public boolean isDeshabilitarEliminar() {
        return deshabilitarEliminar;
    }

    //-------------------
    //GETTERS AND SETTERS
    //-------------------
    public void setDeshabilitarEliminar(boolean deshabilitarEliminar) {    
        this.deshabilitarEliminar = deshabilitarEliminar;
    }

    public boolean isDeshabilitarAnterior() {
        return deshabilitarAnterior;
    }

    public void setDeshabilitarAnterior(boolean deshabilitarAnterior) {
        this.deshabilitarAnterior = deshabilitarAnterior;
    }

    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public ListaDE getListaDE() {
        return listaDE;
    }

    public void setListaDE(ListaDE listaDE) {
        this.listaDE = listaDE;
    }

    public NodoDE getInfanteMostrar() {
        return infanteMostrar;
    }

    public void setInfanteMostrar(NodoDE infanteMostrar) {
        this.infanteMostrar = infanteMostrar;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public boolean isDeshabilitarPrimero() {
        return deshabilitarPrimero;
    }

    public void setDeshabilitarPrimero(boolean deshabilitarPrimero) {
        this.deshabilitarPrimero = deshabilitarPrimero;
    }

    public boolean isDeshabilitarSiguiente() {
        return deshabilitarSiguiente;
    }

    public void setDeshabilitarSiguiente(boolean deshabilitarSiguiente) {
        this.deshabilitarSiguiente = deshabilitarSiguiente;
    }

    public boolean isDeshabilitarUltimo() {
        return deshabilitarUltimo;
    }

    public void setDeshabilitarUltimo(boolean deshabilitarUltimo) {
        this.deshabilitarUltimo = deshabilitarUltimo;
    }

    /**
     * Creates a new instance of BeanListaDE
     */
    public BeanListaDE() {

    }

    @PostConstruct
    public void llenarInfante() {
        listaDE.adicionarNodo(new Infante("Pipe", (byte) 3, 'M'));
        listaDE.adicionarNodo(new Infante("Salome", (byte) 5, 'F'));
        listaDE.adicionarNodo(new Infante("Andres", (byte) 4, 'M'));
        irAlPrimero();
        evaluarBotones();
    }

    public void habilitarCrearInfante() {
        deshabilitarNuevo = false;
        infanteMostrar = new NodoDE(new Infante());
    }

    public void guardarInfante() {
        listaDE.adicionarNodo(infanteMostrar.getDato());
        infanteMostrar = new NodoDE(new Infante());
        deshabilitarNuevo = true;
        irAlPrimero();
        evaluarBotonEliminar();
        JsfUtil.addSuccessMessage("Infante guardado con exito");
    }

    public void guardarInfanteAlInicio() {
        listaDE.adicionarNodoAlInicio(infanteMostrar.getDato());
        infanteMostrar = new NodoDE(new Infante());
        deshabilitarNuevo = true;
        irAlPrimero();
        evaluarBotonEliminar();
        JsfUtil.addSuccessMessage("Infante guardado con exito");
    }

    public void irAlPrimero() {
        infanteMostrar = listaDE.getCabeza();
        if (infanteMostrar == listaDE.getCabeza() || listaDE.contarNodos() == 0) {
            deshabilitarPrimero = true;
            deshabilitarAnterior = true;
            deshabilitarSiguiente = false;
            deshabilitarUltimo = false;
        }
        pintarLista();
    }

    public void irAlAnterior() {
        if (infanteMostrar.getAnterior() != null) {
            infanteMostrar = infanteMostrar.getAnterior();
            deshabilitarSiguiente = false;
            deshabilitarUltimo = false;
            if (infanteMostrar == listaDE.getCabeza()) {
                deshabilitarPrimero = true;
                deshabilitarAnterior = true;
            }
        }
    }

    public void irAlSiguiente() {
        if (infanteMostrar.getSiguiente() != null) {
            infanteMostrar = infanteMostrar.getSiguiente();
            deshabilitarPrimero = false;
            deshabilitarAnterior = false;
            if (infanteMostrar == listaDE.encontrarUltimo()) {
                deshabilitarSiguiente = true;
                deshabilitarUltimo = true;
            }
        }
    }

    public void irAlUltimo() {
        infanteMostrar = listaDE.encontrarUltimo();
        if (infanteMostrar == listaDE.encontrarUltimo() || listaDE.contarNodos() == 0) {
            deshabilitarUltimo = true;
            deshabilitarSiguiente = true;
            deshabilitarPrimero = false;
            deshabilitarAnterior = false;
        }
    }

    public void invertirLista() {
        listaDE.invertirLista();
        irAlPrimero();
    }

    public void cancelarGuardado() {
        deshabilitarNuevo = true;
        irAlPrimero();
    }

    public void eliminarInfante() {
        if (infanteMostrar != null) {
            listaDE.eliminarNodo(infanteMostrar.getDato());
            ///Mostrar mensaje
            JsfUtil.addSuccessMessage("Se ha eliminado con éxito");
            irAlPrimero();
            evaluarBotones();
        } else {
            JsfUtil.addErrorMessage("No hay datos para eliminar");
        }

    }

    public void eliminarXPosicion() {
        listaDE.eliminarNodoXPosicion(posicion);
        irAlPrimero();
        evaluarBotones();
    }

    public void eliminarDondeEstoyParado() {
        listaDE.eliminarDondeEstoyParado(infanteMostrar.getDato());
        JsfUtil.addSuccessMessage("infante eliminado satisfactoriamente");
        irAlPrimero();
        evaluarBotones();
    }
    
    public void evaluarBotonEliminar(){
        if (listaDE.contarNodos() > 0 ) {
            deshabilitarEliminar = false;
        }
    }

    public void evaluarBotones() {
        if (listaDE.contarNodos() == 0 || listaDE.contarNodos() == 1) {
            deshabilitarPrimero = true;
            deshabilitarAnterior = true;
            deshabilitarSiguiente = true;
            deshabilitarUltimo = true;
        }
        if (infanteMostrar == listaDE.getCabeza()) {
            deshabilitarPrimero = true;
            deshabilitarAnterior = true;
        }
        if (listaDE.contarNodos() == 0) {
            deshabilitarEliminar = true;
        }
    }

    public void pintarLista() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);

        StateMachineConnector connector = new StateMachineConnector();
        connector.setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
        connector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
        model.setDefaultConnector(connector);

        //adicionar los elementos 
        if (listaDE.getCabeza() != null) {
            NodoDE temp = listaDE.getCabeza();
            int posX = 5;
            int posY = 2;
            while (temp != null) {
                Element ele = new Element(temp.getDato().getNombre(), posX + "em", posY + "em");

                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_LEFT));

                model.addElement(ele);
                temp = temp.getSiguiente();
                posX = posX + 10;
                posY = posY + 10;
            }
            for (int i = 0; i < model.getElements().size() - 1; i++) {
                model.connect(createConnection(model.getElements().get(i).getEndPoints()
                        .get(1), model.getElements().get(i + 1).getEndPoints().get(0), "Sig"));

                model.connect(createConnection(model.getElements().get(i + 1).getEndPoints()
                        .get(3), model.getElements().get(i).getEndPoints().get(2), "ant"));
            }

        }

    }

    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));
        if (label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }

        return conn;
    }

}
